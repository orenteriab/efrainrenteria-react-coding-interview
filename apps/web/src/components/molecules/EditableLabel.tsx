import { Button, Input, Typography } from "@mui/material";
import { useRef, useState } from "react";

type Props = {
  initValue: string;
  validate?: (value: string) => boolean;
};

export const EditableLabel = ({ initValue, validate }: Props) => {
  const [value, setValue] = useState(initValue ?? "");
  const [edit, setEdit] = useState(false);
  const newNameRef = useRef(null);
  const [error, setError] = useState(false);
  const handleEnableEdit = () => {
    setEdit(true);
  };
  const handleSave = () => {
    if (validate && !validate(newNameRef.current.value)) {
      setError(true);
      return;
    }

    setError(false);
    setValue(newNameRef.current.value);
    setEdit(false);
  };
  const handleCancel = () => setEdit(false);

  return (<>
    {!edit ? <Typography variant="subtitle1" lineHeight="1rem" onClick={handleEnableEdit}>
      {value}
    </Typography> : (
      <>
        <Input inputRef={newNameRef} defaultValue={value} error={error} />
        <Button onClick={handleSave}>Save</Button>
        <Button onClick={handleCancel}>Cancel</Button>
      </>
    )}
  </>)
};